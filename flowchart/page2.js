//a 
const name = "James";

const greet = (name) => {
	return `hello ${name}`;
} 
console.log(greet(name));

//b
const obj1 = {a:1 ,b:2 ,c:3 ,d:4}
for(const property in obj1){
	console.log(`${property}: ${obj1[property]}`);
}


//c
let o1, o2, o3, o4;
o1 = {a: 1};
o2 = {b: 2};
o3 = {c: 3};
o4 = {d: 4};

let obj2 = Object.assign(o1,o2,o3,o4);
console.log(obj2);


//d
const isLesser = (a, b) => {
	return new Promise((resolve, reject) => {
  	let status = "";
    a < b ? status = "lesser" : status = "greater";
    resolve(status);
  })
}

isLesser(1,2).then(console.log);
