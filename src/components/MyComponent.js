import React from 'react';

class MyComponent extends React.Component{
	constructor(){
		super();
		this.state = {
			title: "My React Component!",
			userInput: "",
		};
	}

	render(){
		return <div>
			<h1>{this.state.title}</h1>
		</div>
	}
}

export default MyComponent;
