import classes from './User.module.css';
import {Component} from 'react';


// to add props in old react syntax you may use the Component which you need to import from react then add 'this' keyword.
// functions are replace by class 

class User extends Component {
	render(){
		return <li className={classes.user}>{this.props.name}</li>; 
	}
}
export default User;
