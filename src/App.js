import UserFinder from './components/UserFinder';
import UsersContext from './user-context';
import MyComponent from './components/MyComponent';
const DUMMY_USERS = [
    { id: 'u1', name: 'Max' },
    { id: 'u2', name: 'Manuel' },
    { id: 'u3', name: 'Julie' },
  ];


function App() {
  const usersContext = {
    users: DUMMY_USERS
  }

  return (
    <MyComponent/>
  );
}

export default App;